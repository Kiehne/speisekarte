-- MySQL dump 10.16  Distrib 10.1.22-MariaDB, for Linux (x86_64)
--
-- Host: schtiehve.duckdns.org    Database: speisekarte
-- ------------------------------------------------------
-- Server version	10.1.22-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inhaltindex`
--

DROP TABLE IF EXISTS `inhaltindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inhaltindex` (
  `SpeisenID` int(11) NOT NULL,
  `InhaltsstoffID` int(11) NOT NULL,
  PRIMARY KEY (`SpeisenID`,`InhaltsstoffID`),
  KEY `InhaltsstoffID` (`InhaltsstoffID`),
  CONSTRAINT `inhaltindex_ibfk_1` FOREIGN KEY (`SpeisenID`) REFERENCES `speisekarte` (`ID`),
  CONSTRAINT `inhaltindex_ibfk_2` FOREIGN KEY (`InhaltsstoffID`) REFERENCES `inhaltsstoffe` (`InhaltsstoffID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inhaltindex`
--

LOCK TABLES `inhaltindex` WRITE;
/*!40000 ALTER TABLE `inhaltindex` DISABLE KEYS */;
INSERT INTO `inhaltindex` VALUES (2,2),(3,1);
/*!40000 ALTER TABLE `inhaltindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inhaltsstoffe`
--

DROP TABLE IF EXISTS `inhaltsstoffe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inhaltsstoffe` (
  `InhaltsstoffID` int(11) NOT NULL,
  `Inhaltsstoff` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`InhaltsstoffID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inhaltsstoffe`
--

LOCK TABLES `inhaltsstoffe` WRITE;
/*!40000 ALTER TABLE `inhaltsstoffe` DISABLE KEYS */;
INSERT INTO `inhaltsstoffe` VALUES (1,'Laktose'),(2,'Gluten');
/*!40000 ALTER TABLE `inhaltsstoffe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategorien`
--

DROP TABLE IF EXISTS `kategorien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategorien` (
  `KategorieID` int(11) NOT NULL,
  `Kategorie` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`KategorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategorien`
--

LOCK TABLES `kategorien` WRITE;
/*!40000 ALTER TABLE `kategorien` DISABLE KEYS */;
INSERT INTO `kategorien` VALUES (1,'Vorspeise'),(2,'Hauptgericht'),(3,'Dessert'),(4,'Salat'),(5,'Fleisch'),(6,'Fisch');
/*!40000 ALTER TABLE `kategorien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `katindex`
--

DROP TABLE IF EXISTS `katindex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `katindex` (
  `SpeisenID` int(11) NOT NULL,
  `KategorieID` int(11) NOT NULL,
  PRIMARY KEY (`SpeisenID`,`KategorieID`),
  KEY `KategorieID` (`KategorieID`),
  CONSTRAINT `katindex_ibfk_1` FOREIGN KEY (`SpeisenID`) REFERENCES `speisekarte` (`ID`),
  CONSTRAINT `katindex_ibfk_2` FOREIGN KEY (`KategorieID`) REFERENCES `kategorien` (`KategorieID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `katindex`
--

LOCK TABLES `katindex` WRITE;
/*!40000 ALTER TABLE `katindex` DISABLE KEYS */;
INSERT INTO `katindex` VALUES (1,1),(1,4),(2,2),(2,5),(3,3);
/*!40000 ALTER TABLE `katindex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speisekarte`
--

DROP TABLE IF EXISTS `speisekarte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `speisekarte` (
  `ID` int(11) NOT NULL,
  `Speise` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Preis` double DEFAULT NULL,
  `Beschreibung` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speisekarte`
--

LOCK TABLES `speisekarte` WRITE;
/*!40000 ALTER TABLE `speisekarte` DISABLE KEYS */;
INSERT INTO `speisekarte` VALUES (1,'Beilagensalat',3.5,'Beilagensalat'),(2,'Wiener Schnitzel',5.7,'Wiener Schnitzel mit Pommes'),(3,'Mouse au Chocolat',2.2,'Mouse au Chocolat'),(4,'Chili con Carne',6.0,'Chili con Carne  mit Fleisch, Bohnen und Creme fraiche'),(5,'Chili sin Carne',5.5,'Chili sin Carne ohne Fleisch mit verschiedenen Bohnen'),(6,'Currywurst',3.5,'Rindscurrywurst mit Br�tchen'),(7,'Rattatouille',5.8,'Italienischer Gem�seeintopf'), (8,'Pizza Magherita',5.0,'Pizza Magherita mit Mozarella und Tomatensauce'),(9,'Pizza Fungi',5.5,'Pizza mit Pilzen'),(10,'Pizza Salami',6.0,'Pizza mit Salami'),(11,'Pizza Hawai',6.5,'Pizza mit Schinken und Ananas'),(12,'Pizza Diavolo',6.5,'Scharfe Pizza mit Peperoni und Sucuk'),(13,'Tacosalat',8,'Gro�er Tacosalat f�r 2 Personen mit Hackfleisch Bohnen und Creme fraiche'),(14,'Tortellini Gorgonzola',6.5,'Tortellini mit Gorgonzola Sauce'),(15,'Schoko Schnecken',6.0,'Frische Weinberg Schnecken mit Schokoladensauce'),(16,'Bruscetta',3.0,'Italienische Brotscheiben mit Belag'),(17,'Fanta',2.5,'Fanta Lemon'),(18,'Coca-Cola',2.5,'Coca-Cola'),(19,'Coke light',2.5,'Coca-Cola ohne Zucker'),(20,'Sprite',2.5,'Sprite'),(21,'Wasser still',2.0,'Wasser ohne Kohlens�ure'),(22,'Rotwein',7.0,'Roter Wein aus Sizilien'),(23,'Wei�wein',5.6,'Wei�er Wein aus dem Rheinland');
/*!40000 ALTER TABLE `speisekarte` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-18 17:19:17
